#ifndef WIFI_MODULE_H
#define WIFI_MODULE_H

#include <esp_event.h>

/**
 * @brief being used to see if the wifi is connected or not.
 * 
 */
typedef enum {
    WIFI_CONNECTED,
    WIFI_DISCONNECTED
} wifi_module_event_t;

ESP_EVENT_DECLARE_BASE(WIFI_MODULE_EVENT);

/**
 * 
 * @brief init_wifi initialises the wifi.
 * It initializes connecting to the wifi.
 * 
 **/
void init_wifi();

#endif