#ifndef LCD_H
#define LCD_H

#include <esp_err.h>
#include <hd44780.h>

/**
 * @struct lcd_info_t
 * @brief Stores all the data the lcd_modules needs.
 * 
 * @param init boolean which contains if it has been initialised.
 * @param hd47780_handle contains data of the lcd screen.
 *  @param[in] i2c_port I2C port that will be used.
 *  @param[in] i2c_address I2C address of the QWIIC TWIST module.
 *  @param[in] semaphore I2C semaphore handle to enable thread safe operations.
 * 
 */
typedef struct
{
    bool init;
    hd44780_t hd47780_handle;
    uint8_t i2c_port;
    uint8_t i2c_address;
    SemaphoreHandle_t *semaphore;
} lcd_info_t;

/**
 *  @brief  Construct a new lcd info instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new device info instance, or NULL if it cannot be created.
 */
lcd_info_t *lcd_malloc(void);

/**
 *  @brief  Delete an existing lcd info instance.
 *  @param lcd_info Pointer to lcd info instance that will be freed and set to NULL.
 */
void lcd_free(lcd_info_t **lcd_info);

/**
 * @brief Initialises the lcd_module.
 * 
 * @param lcd_info lcd info instance that will be initialized.
 *  @param i2c_port I2C port that will be used.
 *  @param i2c_address I2C address of the lcd module.
 *  @param semaphore I2C semaphore handle to enable thread safe operations.
 *  @return Error type, should be ESP_OK when successful.
 */
esp_err_t lcd_init(lcd_info_t *lcd_info, uint8_t i2c_port, uint8_t i2c_address, SemaphoreHandle_t *semaphore);

/**
 * @brief This method writes a character on the lcd screen.
 * 
 * @param lcd_info contains the info of the lcd screen needed.
 * @param character The character which will be writen down.
 * @return esp_err_t returns ESP_OK.
 */
esp_err_t lcd_write_character(lcd_info_t *lcd_info, char character);

/**
 * @brief This method writes characters on the lcd screen.
 * 
 * @param lcd_info contains the info of the lcd screen needed.
 * @param characters The characters which will be written down.
 * @param amount is the size of the  char *characters.
 * @return esp_err_t returns ESP_OK
 */
esp_err_t lcd_write_characters(lcd_info_t *lcd_info, char *characters, int amount);

/**
 * @brief This method writes a string on the lcd screen.
 * 
 * @param lcd_info contains the info of the lcd screen needed.
 * @param string The string which will be written down.
 * @return esp_err_t returns ESP_OK.
 */
esp_err_t lcd_write_string(lcd_info_t *lcd_info, char *string);

/**
 * @brief This method places the cursor on the wanted place using x and y.
 * 
 * @param lcd_info contains the info of the lcd screen needed.
 * @param x the x value of the place where the user wants to write.
 * @param y the y value of the place where the user wants to write.
 * @return esp_err_t returns ESP_OK.
 */
esp_err_t lcd_goto(lcd_info_t *lcd_info, uint8_t x, uint8_t y);

#endif