#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_sntp.h"
#include "esp_http_client.h"
#include "cJSON.h"

#include <ip_module.h>
#include <wifi_module.h>

ESP_EVENT_DEFINE_BASE(IP_MODULE_EVENT);

static void fetch_data_task();
static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

static const char *TAG = "IP module";

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == WIFI_MODULE_EVENT && event_id == WIFI_CONNECTED)
    {
        // start the ip data fetching task with the lowest priority
        xTaskCreate(fetch_data_task, "getIpDataTask", 4096, NULL, tskIDLE_PRIORITY, NULL);
    }
}

static esp_err_t fetch_data_task_event_handler(esp_http_client_event_t *evt)
{
    // create buffer for full output message
    static char *output_buffer;
    static int output_len;

    //checks if there is data available
    if (evt->event_id == HTTP_EVENT_ON_DATA)
    {
        if (!esp_http_client_is_chunked_response(evt->client))
        {
            if (output_buffer == NULL)
            {
                // initialize output buffer
                output_buffer = (char *)malloc(esp_http_client_get_content_length(evt->client));
                output_len = 0;
                if (output_buffer == NULL)
                {
                    ESP_LOGE(TAG, "Failed to allocate memory for output buffer");
                    return ESP_FAIL;
                }
            }

            // copy new data to output buffer
            memcpy(output_buffer + output_len, evt->data, evt->data_len);
            output_len += evt->data_len;
        }
    }
    //checks if http is finished
    else if (evt->event_id == HTTP_EVENT_ON_FINISH)
    {
        if (output_buffer != NULL)
        {
            const cJSON *city_json = NULL;
            const cJSON *offset_json = NULL;

            // parse output buffer to json object
            cJSON *root = cJSON_Parse(output_buffer);

            // fetch city string from root object
            city_json = cJSON_GetObjectItemCaseSensitive(root, "city");
            if (!cJSON_IsString(city_json))
            {
                ESP_LOGE(TAG, "Parsing error: city is not a string");
            }

            // fetch offset number from root object
            offset_json = cJSON_GetObjectItemCaseSensitive(root, "offset");
            if (!cJSON_IsNumber(offset_json))
            {
                ESP_LOGE(TAG, "Parsing error: offset is not a number");
            }

            // create event data
            ip_data_t ip_data = {
                .city = city_json->valuestring,
                .offset = offset_json->valueint};

            // post IP data event to the default event loop
            ESP_ERROR_CHECK(esp_event_post(IP_MODULE_EVENT, IP_DATA_FETCHED, &ip_data, sizeof(ip_data_t), portMAX_DELAY));
            ESP_LOGD(TAG, "Response = City: %s, Offset: %d", ip_data.city, ip_data.offset);

            // free output buffer
            free(output_buffer);
            output_buffer = NULL;
        }
        output_len = 0;
    }
    //checks if http disconnected
    else if (evt->event_id == HTTP_EVENT_DISCONNECTED)
    {
        ESP_LOGI(TAG, "HTTP_EVENT_DISCONNECTED");
        if (output_buffer != NULL)
        {
            // free output buffer
            free(output_buffer);
            output_buffer = NULL;
        }
    }

    return ESP_OK;
}

static void fetch_data_task()
{
    // initialize HTTP request
    esp_http_client_config_t config = {
        .url = "http://ip-api.com/json/?fields=status,message,city,offset",
        .event_handler = fetch_data_task_event_handler};
    esp_http_client_handle_t client = esp_http_client_init(&config);

    // perform HTTP request
    esp_err_t err = esp_http_client_perform(client);

    if (err == ESP_OK)
    {
        int status_code = esp_http_client_get_status_code(client);
        int content_length = esp_http_client_get_content_length(client);

        ESP_LOGI(TAG, "Status = %d, content_length = %d", status_code, content_length);
    }

    // cleanup HTTP request
    esp_http_client_cleanup(client);
    vTaskDelete(NULL);
}

void init_ip_module()
{
    // register to WiFi events
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
}