#ifndef IP_MODULE_H
#define IP_MODULE_H

#include <esp_event.h>
/**
 * @brief These enums are used to define an event_id.
 * 
 */
typedef enum
{
    IP_DATA_FETCHED
} ip_module_event_t;

/**
 * @struct ip_data_t
 * @brief Stores all the data the ip_module needs.
 * 
 * @param city This is the location.
 * @param offset Differnce in time.
 */
typedef struct
{
    char *city;
    int offset;
} ip_data_t;

ESP_EVENT_DECLARE_BASE(IP_MODULE_EVENT);

/**
 * @brief Initializes the ip_module.
 * 
 */
void init_ip_module();

#endif