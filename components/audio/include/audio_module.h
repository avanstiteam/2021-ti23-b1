#ifndef AUDIO_MODULE_H
#define AUDIO_MODULE_H

#include <esp_peripherals.h>
#include <esp_event.h>
#include <esp_err.h>
#include <board.h>
#include <time.h>

/**
 * @brief These enums are used to define an event_id.
 * 
 */
typedef enum
{
    VOLUME_CHANGED,
    VOLUME_MUTED,
    NO_AUDIO
} audio_module_event_t;

ESP_EVENT_DECLARE_BASE(AUDIO_MODULE_EVENT);

/**
 * @brief Plays an mp3 file.
 * 
 * @param mp3_name Name of the mp3 file.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t play_mp3(char *mp3_name);
esp_err_t play_mp3_continuous(char *mp3_name);

/**
 * @brief Plays an audiostream from an url.
 * 
 * @param http_url Url of the stream.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t play_http(char *http_url);

/**
 * @brief Plays input text on speakers.
 * 
 * @param text The text that gets spoken.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t play_text_to_speach(char **text);

/**
 * @brief Closes current pipeline.
 * 
 * @return Error type, should be ESP_OK when successful. 
 */
esp_err_t stop_audio();

/**
 * @brief Sets the volume of the device.
 * 
 * @param volume Value of the volume
 * @return Error type, should be ESP_OK when successful. 
 */
esp_err_t set_volume(int volume);

/**
 * @brief Gets the current volume of the device.
 * 
 * @return Current volume of the device.
 */
int get_volume();

/**
 * @brief Mutes the device
 * 
 * @param mute ??
 * @return Error type, should be ESP_OK when successful. 
 */
esp_err_t mute(int mute);

/**
 * @brief Initializes the audio_module, and creates mutexes.
 * 
 * @param board_handle Is being used for the Audio_hal.
 * @param periph_handle Is being used for the event listener.
 * @return Error type, should be ESP_OK when successful. 
 */
esp_err_t init_audio_module(audio_board_handle_t board_handle, esp_periph_set_handle_t periph_handle);

//MEMO
esp_err_t record_memo();
esp_err_t play_memo(char *memo_name);
#endif