#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_event.h"
#include "esp_log.h"
#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "http_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"
#include "esp_peripherals.h"
#include "periph_sdcard.h"
#include "board.h"
#include "wav_decoder.h"
#include "wav_encoder.h"

#include "audio_module.h"
#include <input.h>

ESP_EVENT_DEFINE_BASE(AUDIO_MODULE_EVENT);

static const char *TAG = "Audio module";

static esp_periph_set_handle_t set;
static audio_board_handle_t board;

static SemaphoreHandle_t play_mutex;
static SemaphoreHandle_t setup_mutex;
static SemaphoreHandle_t volume_mutex;
static TaskHandle_t audio_task;

static audio_pipeline_handle_t pipeline;
static audio_event_iface_handle_t event;

static audio_element_handle_t *audio_elements[3];
static char *audio_tags[3];

static int est_volume = 0;
static bool request_stop = false;
static bool request_play = false;

static char date_time_string[100];

static void _event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    if (event_base == INPUT_MODULE_EVENT && event_id == VOL_DOWN_PRESSED)
    {
        set_volume(est_volume - 10);
    }
    else if (event_base == INPUT_MODULE_EVENT && event_id == VOL_UP_PRESSED)
    {
        set_volume(est_volume + 10);
    }
}

static audio_element_handle_t _create_mp3_file_reader(const char *file)
{
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    audio_element_handle_t fatfs_stream = fatfs_stream_init(&fatfs_cfg);

    char uri[100];
    sprintf(uri, "/sdcard/%s.mp3", file);

    ESP_ERROR_CHECK(audio_element_set_uri(fatfs_stream, uri));

    return fatfs_stream;
}

static audio_element_handle_t _create_http_stream_reader(char *url)
{
    http_stream_cfg_t http_cfg = HTTP_STREAM_CFG_DEFAULT();
    audio_element_handle_t http_stream = http_stream_init(&http_cfg);
    audio_element_set_uri(http_stream, url);
    return http_stream;
}

static audio_element_handle_t _create_i2s_writer_stream()
{
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_WRITER;
    return i2s_stream_init(&i2s_cfg);
}

static audio_element_handle_t _create_mp3_decoder()
{
    mp3_decoder_cfg_t mp3_cfg = DEFAULT_MP3_DECODER_CONFIG();
    return mp3_decoder_init(&mp3_cfg);
}

void _pipeline_init()
{
    audio_pipeline_cfg_t pipeline_cfg = DEFAULT_AUDIO_PIPELINE_CONFIG();
    pipeline = audio_pipeline_init(&pipeline_cfg);

    for (int i = 0; i < 3; i++)
    {
        ESP_ERROR_CHECK(audio_pipeline_register(pipeline, *audio_elements[i], audio_tags[i]));
    }

    ESP_ERROR_CHECK(audio_pipeline_link(pipeline, (const char **)audio_tags, 3));
}

static void _pipeline_deinit()
{
    audio_pipeline_stop(pipeline);
    audio_pipeline_wait_for_stop(pipeline);
    audio_pipeline_terminate(pipeline);

    for (int i = 0; i < 3; i++)
    {
        ESP_ERROR_CHECK(audio_pipeline_unregister(pipeline, *audio_elements[i]));
        audio_element_wait_for_stop(*audio_elements[i]);
        ESP_ERROR_CHECK(audio_element_deinit(*audio_elements[i]));
    }

    ESP_ERROR_CHECK(audio_pipeline_remove_listener(pipeline));
    ESP_ERROR_CHECK(audio_pipeline_deinit(pipeline));
}

static void _event_init()
{
    audio_event_iface_cfg_t evt_cfg = AUDIO_EVENT_IFACE_DEFAULT_CFG();
    event = audio_event_iface_init(&evt_cfg);
    ESP_ERROR_CHECK(audio_pipeline_set_listener(pipeline, event));
    ESP_ERROR_CHECK(audio_event_iface_set_listener(esp_periph_set_get_event_iface(set), event));
}

static void _event_deinit()
{
    audio_event_iface_remove_listener(esp_periph_set_get_event_iface(set), event);
    audio_event_iface_destroy(event);
}

static void _listen_mp3_stream(audio_element_handle_t *mp3_decoder, audio_element_handle_t *i2s_stream)
{
    audio_pipeline_run(pipeline);

    while (1)
    {
        audio_event_iface_msg_t msg;
        esp_err_t error = audio_event_iface_listen(event, &msg, portMAX_DELAY);
        if (error != ESP_OK)
        {
            ESP_LOGE(TAG, "Event interface error : %d", error);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)*mp3_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            audio_element_info_t music_info = {0};
            audio_element_getinfo(*mp3_decoder, &music_info);

            ESP_LOGI(TAG, "Receive music info from mp3 decoder, sample_rates=%d, bits=%d, ch=%d",
                     music_info.sample_rates, music_info.bits, music_info.channels);

            audio_element_setinfo(*i2s_stream, &music_info);
            i2s_stream_set_clk(*i2s_stream, music_info.sample_rates, music_info.bits, music_info.channels);
            continue;
        }

        /* Stop when the last pipeline element (i2s_stream_writer in this case) receives stop event */
        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)*i2s_stream && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED)))
        {
            ESP_LOGW(TAG, "Stop event received");
            break;
        }
    }
}

void play_mp3_task(void *data)
{
    char *mp3_name = (char *)data;

    request_play = true;
    stop_audio();

    xSemaphoreTake(play_mutex, portMAX_DELAY);
    xSemaphoreTake(setup_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Play %s", mp3_name);

    audio_element_handle_t fatfs_stream = _create_mp3_file_reader(mp3_name);
    audio_element_handle_t mp3_decoder = _create_mp3_decoder();
    audio_element_handle_t i2s_stream = _create_i2s_writer_stream();

    audio_elements[0] = &fatfs_stream;
    audio_elements[1] = &mp3_decoder;
    audio_elements[2] = &i2s_stream;

    audio_tags[0] = "file";
    audio_tags[1] = "mp3";
    audio_tags[2] = "i2s";

    _pipeline_init();
    _event_init();

    xSemaphoreGive(setup_mutex);
    _listen_mp3_stream(&mp3_decoder, &i2s_stream);
    _pipeline_deinit();
    _event_deinit();

    pipeline = NULL;

    if (!request_play)
    {
        ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, NO_AUDIO, NULL, 0, portMAX_DELAY));
    }

    request_play = false;

    xSemaphoreGive(play_mutex);
    vTaskDelete(NULL);
}

void play_mp3_continuous_task(void *data)
{
    char *mp3_name = (char *)data;

    request_play = true;
    stop_audio();

    xSemaphoreTake(play_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Play %s continuously", mp3_name);

    while (1)
    {
        xSemaphoreTake(setup_mutex, portMAX_DELAY);

        audio_element_handle_t fatfs_stream = _create_mp3_file_reader(mp3_name);
        audio_element_handle_t mp3_decoder = _create_mp3_decoder();
        audio_element_handle_t i2s_stream = _create_i2s_writer_stream();

        audio_elements[0] = &fatfs_stream;
        audio_elements[1] = &mp3_decoder;
        audio_elements[2] = &i2s_stream;

        audio_tags[0] = "file";
        audio_tags[1] = "mp3";
        audio_tags[2] = "i2s";

        _pipeline_init();
        _event_init();

        xSemaphoreGive(setup_mutex);
        _listen_mp3_stream(&mp3_decoder, &i2s_stream);
        _pipeline_deinit();
        _event_deinit();

        if (request_stop)
        {
            break;
        }
    }

    pipeline = NULL;

    if (!request_play)
    {
        ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, NO_AUDIO, NULL, 0, portMAX_DELAY));
    }

    request_play = false;

    xSemaphoreGive(play_mutex);
    vTaskDelete(NULL);
}

void play_http_task(void *data)
{
    char *http_url = (char *)data;

    request_play = true;
    stop_audio();

    xSemaphoreTake(setup_mutex, portMAX_DELAY);
    xSemaphoreTake(play_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Play %s", http_url);

    audio_element_handle_t http_stream = _create_http_stream_reader(http_url);
    audio_element_handle_t mp3_decoder = _create_mp3_decoder();
    audio_element_handle_t i2s_stream = _create_i2s_writer_stream();

    audio_elements[0] = &http_stream;
    audio_elements[1] = &mp3_decoder;
    audio_elements[2] = &i2s_stream;

    audio_tags[0] = "http";
    audio_tags[1] = "mp3";
    audio_tags[2] = "i2s";

    _pipeline_init();
    _event_init();

    xSemaphoreGive(setup_mutex);
    _listen_mp3_stream(&mp3_decoder, &i2s_stream);
    _pipeline_deinit();
    _event_deinit();

    pipeline = NULL;

    if (!request_play)
    {
        ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, NO_AUDIO, NULL, 0, portMAX_DELAY));
    }

    request_play = false;

    xSemaphoreGive(play_mutex);
    vTaskDelete(NULL);
}

void play_tts_task(void *data)
{
    char **mp3_names = (char **)data;

    stop_audio();

    xSemaphoreTake(play_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Start tts!");
    for (int i = 0; mp3_names[i] != NULL; i++)
    {
        xSemaphoreTake(setup_mutex, portMAX_DELAY);

        char *mp3_name = mp3_names[i];

        ESP_LOGI(TAG, "Play %s", mp3_name);

        audio_element_handle_t fatfs_stream = _create_mp3_file_reader(mp3_name);
        audio_element_handle_t mp3_decoder = _create_mp3_decoder();
        audio_element_handle_t i2s_stream = _create_i2s_writer_stream();

        audio_elements[0] = &fatfs_stream;
        audio_elements[1] = &mp3_decoder;
        audio_elements[2] = &i2s_stream;

        audio_tags[0] = "file";
        audio_tags[1] = "mp3";
        audio_tags[2] = "i2s";

        _pipeline_init();
        _event_init();

        xSemaphoreGive(setup_mutex);
        _listen_mp3_stream(&mp3_decoder, &i2s_stream);
        _pipeline_deinit();
        _event_deinit();

        pipeline = NULL;

        if (request_stop)
        {
            break;
        }

        // free(mp3_name); // potential memory leak
    }

    ESP_LOGI(TAG, "Stop tts!");
    // free(mp3_names); // potential memory leak

    if (!request_play)
    {
        ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, NO_AUDIO, NULL, 0, portMAX_DELAY));
    }

    request_play = false;

    xSemaphoreGive(play_mutex);
    vTaskDelete(NULL);
}

esp_err_t play_text_to_speach(char **text)
{
    xTaskCreate(play_tts_task, "playTextToSpeach", 4096, text, 3, &audio_task);
    return ESP_OK;
}

esp_err_t play_mp3(char *mp3_name)
{
    xTaskCreate(play_mp3_task, "playMp3Task", 4096, mp3_name, 3, &audio_task);
    return ESP_OK;
}

esp_err_t play_mp3_continuous(char *mp3_name)
{
    xTaskCreate(play_mp3_continuous_task, "playMp3ContinuousTask", 4096, mp3_name, 3, &audio_task);
    return ESP_OK;
}

esp_err_t play_http(char *http_url)
{
    xTaskCreate(play_http_task, "playHttpTask", 4096, http_url, 3, &audio_task);
    return ESP_OK;
}

esp_err_t stop_audio()
{
    ESP_LOGI(TAG, "Await setup");

    xSemaphoreTake(setup_mutex, portMAX_DELAY);

    request_stop = true;

    if (pipeline != NULL)
    {
        ESP_LOGI(TAG, "Stop audio");
        audio_pipeline_stop(pipeline);
        audio_pipeline_wait_for_stop(pipeline);
    }

    xSemaphoreTake(play_mutex, portMAX_DELAY);

    request_stop = false;

    xSemaphoreGive(setup_mutex);
    xSemaphoreGive(play_mutex);

    return ESP_OK;
}

esp_err_t set_volume(int volume)
{
    if (volume < 0)
        volume = 0;
    if (volume > 100)
        volume = 100;

    xSemaphoreTake(volume_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Set volume %d", volume);

    ESP_ERROR_CHECK(audio_hal_set_volume(board->audio_hal, volume));
    est_volume = volume;

    xSemaphoreGive(volume_mutex);

    int new_v = volume;
    ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, VOLUME_CHANGED, &new_v, sizeof(int), portMAX_DELAY));

    return ESP_OK;
}

int get_volume()
{
    xSemaphoreTake(volume_mutex, portMAX_DELAY);

    int volume;
    ESP_ERROR_CHECK(audio_hal_get_volume(board->audio_hal, &volume));

    xSemaphoreGive(volume_mutex);

    return volume;
}

esp_err_t mute(int mute)
{
    xSemaphoreTake(volume_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Set mute %d", mute);

    ESP_ERROR_CHECK(audio_hal_set_mute(board->audio_hal, mute));

    xSemaphoreGive(volume_mutex);

    ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, VOLUME_MUTED, &mute, sizeof(int), portMAX_DELAY));

    return ESP_OK;
}

//MEMO
static audio_element_handle_t _create_wav_file_reader(const char *file_name)
{
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_READER;
    audio_element_handle_t fatfs_stream = fatfs_stream_init(&fatfs_cfg);

    char uri[200];
    sprintf(uri, "/sdcard/memos/%s.wav", file_name);

    ESP_ERROR_CHECK(audio_element_set_uri(fatfs_stream, uri));

    return fatfs_stream;
}

//TODO return error if sd card not inserted.
static audio_element_handle_t _create_wav_file_writer()
{
    fatfs_stream_cfg_t fatfs_cfg = FATFS_STREAM_CFG_DEFAULT();
    fatfs_cfg.type = AUDIO_STREAM_WRITER;
    audio_element_handle_t fatfs_stream = fatfs_stream_init(&fatfs_cfg);

    char uri[200];
    sprintf(uri, "/sdcard/memos/%s.wav", date_time_string);

    ESP_ERROR_CHECK(audio_element_set_uri(fatfs_stream, uri));

    return fatfs_stream;
}

static audio_element_handle_t _create_i2s_stream()
{
    i2s_stream_cfg_t i2s_cfg = I2S_STREAM_CFG_DEFAULT();
    i2s_cfg.type = AUDIO_STREAM_READER;
    audio_element_handle_t i2s_stream = i2s_stream_init(&i2s_cfg);
    return i2s_stream;
}

static audio_element_handle_t _create_wav_encoder()
{
    wav_encoder_cfg_t wav_encoder_cfg = DEFAULT_WAV_ENCODER_CONFIG();
    return wav_encoder_init(&wav_encoder_cfg);
}

static audio_element_handle_t _create_wav_decoder()
{
    wav_decoder_cfg_t wav_decoder_cfg = DEFAULT_WAV_DECODER_CONFIG();
    return wav_decoder_init(&wav_decoder_cfg);
}

static void _write_memo_stream(audio_element_handle_t *wav_encoder, audio_element_handle_t *i2s_stream)
{
    audio_pipeline_run(pipeline);

    int seconds_recorded = 0;

    while (1)
    {
        audio_event_iface_msg_t msg;
        if (audio_event_iface_listen(event, &msg, 1000 / portTICK_RATE_MS) != ESP_OK)
        {
            seconds_recorded += 1;

            ESP_LOGI(TAG, "Recording memo, second: %d", seconds_recorded);

            if (seconds_recorded >= 10)
            {
                break;
            }
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)i2s_stream && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED)))
        {
            ESP_LOGW(TAG, "Stop event received");
            break;
        }
    }
}

void record_memo_task()
{
    request_play = true;
    stop_audio();

    xSemaphoreTake(play_mutex, portMAX_DELAY);
    xSemaphoreTake(setup_mutex, portMAX_DELAY);

    audio_element_handle_t fatfs_writer_stream = _create_wav_file_writer();
    audio_element_handle_t i2s_stream = _create_i2s_stream();
    audio_element_handle_t wav_encoder = _create_wav_encoder();

    audio_elements[0] = &i2s_stream;
    audio_elements[1] = &wav_encoder;
    audio_elements[2] = &fatfs_writer_stream;
    ESP_LOGI(TAG, "Audio Elements added.");
    audio_tags[0] = "i2s";
    audio_tags[1] = "wav_encoder";
    audio_tags[2] = "fatfs";
    ESP_LOGI(TAG, "Audio tags added.");

    _pipeline_init();
    _event_init();
    xSemaphoreGive(setup_mutex);

    _write_memo_stream(&wav_encoder, &i2s_stream);

    _pipeline_deinit();
    _event_deinit();

    pipeline = NULL;

    //Directly after closing pipeline, create a new pipeline. If request_play is false / queue is empty, then post event
    if (!request_play)
    {
        ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, NO_AUDIO, NULL, 0, portMAX_DELAY));
    }

    request_play = false;

    xSemaphoreGive(play_mutex);
    vTaskDelete(NULL);
}

esp_err_t record_memo()
{
    time_t rawtime;
    time(&rawtime);

    struct tm *tm;
    tm = localtime(&rawtime);

    strftime(date_time_string, sizeof(date_time_string) - 1, "%H_%M_%S_%d_%m", tm);

    xTaskCreate(record_memo_task, "recordMemoTask", 4096, NULL, 3, NULL);
    return ESP_OK;
}

static void _read_memo_stream(audio_element_handle_t *wav_decoder, audio_element_handle_t *i2s_stream)
{
    audio_pipeline_run(pipeline);

    while (1)
    {
        audio_event_iface_msg_t msg;
        esp_err_t error = audio_event_iface_listen(event, &msg, portMAX_DELAY);
        if (error != ESP_OK)
        {
            ESP_LOGE(TAG, "Event interface error : %d", error);
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)*wav_decoder && msg.cmd == AEL_MSG_CMD_REPORT_MUSIC_INFO)
        {
            audio_element_info_t sound_info = {0};
            audio_element_getinfo(*wav_decoder, &sound_info);
            audio_element_setinfo(*i2s_stream, &sound_info);
            i2s_stream_set_clk(*i2s_stream, sound_info.sample_rates, sound_info.bits, sound_info.channels);
            continue;
        }

        if (msg.source_type == AUDIO_ELEMENT_TYPE_ELEMENT && msg.source == (void *)*i2s_stream && msg.cmd == AEL_MSG_CMD_REPORT_STATUS && (((int)msg.data == AEL_STATUS_STATE_STOPPED) || ((int)msg.data == AEL_STATUS_STATE_FINISHED)))
        {
            break;
        }
    }
}

void play_memo_task(void *memo_name)
{
    char *save_name = (char *)memo_name;

    request_play = true;
    stop_audio();

    xSemaphoreTake(play_mutex, portMAX_DELAY);
    xSemaphoreTake(setup_mutex, portMAX_DELAY);

    ESP_LOGI(TAG, "Started reading memo %s.", save_name);

    audio_element_handle_t fatfs_reader_stream = _create_wav_file_reader(save_name);

    if (fatfs_reader_stream == NULL)
    {
        ESP_LOGI(TAG, "------\nFILE NOT IN FILESYSTEM.\n------");
        xSemaphoreGive(play_mutex);
        xSemaphoreGive(setup_mutex);
        vTaskDelete(NULL);
    }

    audio_element_handle_t wav_decoder = _create_wav_decoder();
    audio_element_handle_t i2s_stream = _create_i2s_writer_stream();

    audio_elements[0] = &fatfs_reader_stream;
    audio_elements[1] = &wav_decoder;
    audio_elements[2] = &i2s_stream;
    ESP_LOGI(TAG, "Audio Elements added.");
    audio_tags[0] = "i2s";
    audio_tags[1] = "wav_decoder";
    audio_tags[2] = "fatfs";
    ESP_LOGI(TAG, "Audio tags added.");

    _pipeline_init();
    _event_init();

    xSemaphoreGive(setup_mutex);

    _read_memo_stream(&wav_decoder, &i2s_stream);
    _pipeline_deinit();
    _event_deinit();

    pipeline = NULL;

    //Directly after closing pipeline, create a new pipeline. If request_play is false / queue is empty, then post event
    if (!request_play)
    {
        ESP_ERROR_CHECK(esp_event_post(AUDIO_MODULE_EVENT, NO_AUDIO, NULL, 0, portMAX_DELAY));
    }

    request_play = false;
    xSemaphoreGive(play_mutex);

    vTaskDelete(NULL);
}

esp_err_t play_memo(char *memo_name)
{
    xTaskCreate(play_memo_task, "playMemoTask", 4096, memo_name, 3, NULL);
    return ESP_OK;
}

esp_err_t init_audio_module(audio_board_handle_t board_handle, esp_periph_set_handle_t periph_handle)
{
    ESP_LOGI(TAG, "Initialize codec");
    ESP_ERROR_CHECK(audio_hal_ctrl_codec(board_handle->audio_hal, AUDIO_HAL_CODEC_MODE_DECODE, AUDIO_HAL_CTRL_START));
    // ESP_LOGI(TAG, "Set volume to 30");
    // ESP_ERROR_CHECK(audio_hal_set_volume(board_handle->audio_hal, 30));

    board = board_handle;
    set = periph_handle;
    play_mutex = xSemaphoreCreateBinary();
    setup_mutex = xSemaphoreCreateBinary();
    volume_mutex = xSemaphoreCreateBinary();

    ESP_ERROR_CHECK(esp_event_handler_register(INPUT_MODULE_EVENT, ESP_EVENT_ANY_ID, _event_handler, NULL));

    xSemaphoreGive(play_mutex);
    xSemaphoreGive(setup_mutex);
    xSemaphoreGive(volume_mutex);

    return ESP_OK;
}