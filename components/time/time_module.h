#ifndef TIME_MODULE_H
#define TIME_MODULE_H

#include "esp_event.h"

/**
 * @brief Enumeration that shows if the Time has been Synced.
 * 
 */
typedef enum {
    TIME_SYNCED
} time_module_event_t;

ESP_EVENT_DECLARE_BASE(TIME_MODULE_EVENT);

/**
 * 
 * @brief init_time_module initialises the time_module. It adds this module toe the EventLoop so when the 
 * program goes through the loop is also refreshes the time.
 * 
 */
void init_time_module();

#endif