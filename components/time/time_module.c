#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <string.h>
#include <time.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_sntp.h"

#include "time_module.h"
#include "ip_module.h"
#include "wifi_module.h"

ESP_EVENT_DEFINE_BASE(TIME_MODULE_EVENT);

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
static void set_timezone(int offset);
static void sntp_sync_callback(struct timeval *tv);

static const char *TAG = "Time module";

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data)
{
    static int sntp_started = 0;

    //checks if the ip has been fetched
    if (event_base == IP_MODULE_EVENT && event_id == IP_DATA_FETCHED)
    {
        // get the IP event data
        ip_data_t *ip_data = (ip_data_t *)event_data;

        // set timezone with UTC offset fetched with the IP
        set_timezone(ip_data->offset);

        if (!sntp_started)
        {
            ESP_LOGI(TAG, "Start SNTP");
            sntp_setoperatingmode(SNTP_OPMODE_POLL);
            sntp_setservername(0, "ntp.time.nl");
            sntp_set_time_sync_notification_cb(sntp_sync_callback);
            sntp_init();

            sntp_started = 1;
        }
    }
    //checks if the wifi was disconnected to stop the sntp.
    else if (event_base == WIFI_MODULE_EVENT && event_id == WIFI_DISCONNECTED)
    {
        if (sntp_started)
        {
            ESP_LOGI(TAG, "Stop SNTP");
            sntp_stop();
            sntp_started = 0;
        }
    }
}

static void sntp_sync_callback(struct timeval *tv)
{
    ESP_LOGI(TAG, "SNTP Time synced!");

    // string manipulation to print the date and the time
    char text[100];
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    strftime(text, sizeof(text) - 1, "%d %m %Y %H:%M", t);
    ESP_LOGD(TAG, "%s", text);

    ESP_ERROR_CHECK(esp_event_post(TIME_MODULE_EVENT, TIME_SYNCED, NULL, 0, portMAX_DELAY));
}

static void set_timezone(int offset)
{
    ESP_LOGD(TAG, "Set timezone");

    // convert seconds to hours
    int offset_hours = offset / 3600;

    // annoying string manipulation to set UTC string with the right offset
    char *utc_string = (char *)malloc(snprintf(NULL, 0, "UTC-%d", offset_hours) + 1);
    sprintf(utc_string, "UTC-%d", offset_hours);

    // set the timezone environment variable
    setenv("TZ", utc_string, 1);

    // publish the timezone changes
    tzset();

    // free annoying string manipulation string pointer
    free(utc_string);
}

void init_time_module()
{
    // register to WiFi events
    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
    // register to IP events
    ESP_ERROR_CHECK(esp_event_handler_register(IP_MODULE_EVENT, ESP_EVENT_ANY_ID, event_handler, NULL));
}