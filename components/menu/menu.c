#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <esp_event.h>
#include <esp_log.h>

#include <frame.h>
#include <menu.h>

ESP_EVENT_DEFINE_BASE(MENU_MODULE_EVENT);

static const char TAG[] = "menu";

static void _menu_print(menu_info_t *menu_info);
static menu_node_t *_menu_find(menu_node_t *menu, int id);
static menu_node_t *_menu_get_current(menu_node_t *menu);

static void _menu_print(menu_info_t *menu_info)
{
    assert(menu_info->current->type == MENU);

    menu_node_t *current = _menu_get_current(menu_info->current);

    frame_clear(menu_info->frame);

    int count = 0;

    // traverse the linked list, beginning from the selected node
    while (current != NULL && count != 4)
    {
        char text[20];
        bool is_selected = current->id == *menu_info->current->curr_child_id;

        // Write the menu node to the correct frame line
        sprintf(text, "%s%s", is_selected ? ">" : "-", current->text);
        ESP_LOGI(TAG, "write %d %s to x=%d y=%d", current->id, text, 0, count);
        ESP_ERROR_CHECK(frame_write_string(menu_info->frame, 0, count, text));

        current = current->next;
        count++;
    }

    ESP_ERROR_CHECK(frame_flush(menu_info->frame));
    ESP_ERROR_CHECK(esp_event_post(MENU_MODULE_EVENT, MENU_UPDATED, NULL, 0, portMAX_DELAY));
}

/*
 *  Get a pointer to the menu item matching the id
 */
static menu_node_t *_menu_find(menu_node_t *menu, int id)
{
    assert(menu->type == MENU);

    menu_node_t *current = menu->child;

    // traverse to end of linked list
    while (current != NULL)
    {
        if (current->id == id)
            return current;

        current = current->next;
    }

    return NULL;
}

/*
 *  Get a pointer to the current menu item
 */
static menu_node_t *_menu_get_current(menu_node_t *menu)
{
    return _menu_find(menu, *menu->curr_child_id);
}

menu_info_t *menu_malloc()
{
    menu_info_t *menu_info = (menu_info_t *)malloc(sizeof(menu_info_t));
    if (menu_info != NULL)
    {
        memset(menu_info, 0, sizeof(*menu_info));

        ESP_LOGD(TAG, "malloc menu_info_t %p", menu_info);
    }
    else
    {
        ESP_LOGE(TAG, "malloc menu_info_t failed");
    }

    return menu_info;
}

void menu_free(menu_info_t **menu_info)
{
    if (menu_info != NULL && (*menu_info != NULL))
    {
        ESP_LOGD(TAG, "free menu_info_t %p", *menu_info);

        menu_node_free(&(*menu_info)->root);

        free(*menu_info);
        *menu_info = NULL;
    }
    else
    {
        ESP_LOGE(TAG, "free menu_info_t failed");
    }
}

esp_err_t menu_init(menu_info_t *menu_info, menu_node_t *root, frame_t *frame, int menu_id)
{
    esp_err_t error;

    if (menu_info != NULL)
    {
        menu_info->menu_id = menu_id;
        menu_info->root = root;
        menu_info->current = root;
        menu_info->frame = frame;

        _menu_print(menu_info);

        error = ESP_OK;
    }
    else
    {
        ESP_LOGE(TAG, "menu_info_t is NULL");
    }

    return error;
}

menu_node_t *menu_node_malloc()
{
    menu_node_t *menu_node = (menu_node_t *)malloc(sizeof(menu_node_t));
    if (menu_node != NULL)
    {
        memset(menu_node, 0, sizeof(*menu_node));

        ESP_LOGD(TAG, "malloc menu_node_t %p", menu_node);
    }
    else
    {
        ESP_LOGE(TAG, "malloc menu_node_t failed");
    }

    return menu_node;
}

void menu_node_free(menu_node_t **menu_node)
{
    if (menu_node != NULL && (*menu_node != NULL))
    {
        ESP_LOGD(TAG, "free menu_node_t %p", *menu_node);

        if ((*menu_node)->next != NULL)
        {
            menu_node_t *node = (*menu_node)->next;
            (*menu_node)->next = NULL;
            menu_node_free(&node);
        }

        if ((*menu_node)->child != NULL)
        {
            menu_node_t *node = (*menu_node)->child;
            (*menu_node)->child = NULL;
            menu_node_free(&node);
        }

        // if ((*menu_node)->text != NULL)
        // {
        //     free((*menu_node)->text);
        // }
        free(*menu_node);
        *menu_node = NULL;
    }
    else
    {
        ESP_LOGE(TAG, "free menu_node_t failed");
    }
}

esp_err_t menu_node_init(menu_node_t *menu_node, uint8_t id, menu_node_type_t type, char *text)
{
    esp_err_t error;

    if (menu_node != NULL)
    {
        menu_node->id = id;
        menu_node->type = type;
        menu_node->text = text;

        error = ESP_OK;
    }
    else
    {
        ESP_LOGE(TAG, "menu_node_t is NULL");
    }

    return error;
}

void menu_add_item(menu_node_t *menu, menu_node_t *item)
{
    assert(menu->type == MENU);

    if (menu->child == NULL)
    {
        // add new child
        menu->child = item;
        menu->curr_child_id = &item->id;

        item->parent = menu;
    }
    else
    {
        menu_node_t *current = menu->child;

        // traverse to end of linked list
        while (current->next != NULL)
        {
            current = current->next;
        }

        // add new child to end of linked list
        current->next = item;
        item->prev = current;
        item->parent = menu;
    }
}

int menu_previous(menu_info_t *menu_info)
{
    assert(menu_info->current->type == MENU);

    menu_node_t *current = _menu_get_current(menu_info->current);
    if (current->prev != NULL)
    {
        ESP_LOGI(TAG, "Cycle to previous id: %d", current->prev->id);
        menu_info->current->curr_child_id = &current->prev->id;
        _menu_print(menu_info);
        return current->prev->id;
    }

    return current->id;
}

int menu_next(menu_info_t *menu_info)
{
    assert(menu_info->current->type == MENU);

    menu_node_t *current = _menu_get_current(menu_info->current);
    if (current->next != NULL)
    {
        ESP_LOGI(TAG, "Cycle to next id: %d", current->next->id);
        menu_info->current->curr_child_id = &current->next->id;
        _menu_print(menu_info);
        return current->next->id;
    }
    return current->id;
}

int menu_select(menu_info_t *menu_info)
{
    menu_node_t *current_child = _menu_get_current(menu_info->current);

    if (current_child != NULL && current_child->type == MENU)
    {
        ESP_LOGI(TAG, "Selecting id: %d", current_child->id);
        menu_info->current = current_child;
        _menu_print(menu_info);
        return current_child->id;
    }
    else if (current_child != NULL && current_child->type == ITEM)
    {
        ESP_LOGI(TAG, "Selecting id: %d", current_child->id);

        menu_item_selected_event_t event = {
            .menu_id = menu_info->menu_id,
            .item_id = current_child->id};
        ESP_ERROR_CHECK(esp_event_post(MENU_MODULE_EVENT, MENU_ITEM_SELECTED, &event, sizeof(menu_item_selected_event_t), portMAX_DELAY));
        return current_child->id;
    }
    return -1;
}

int menu_deselect(menu_info_t *menu_info)
{
    menu_info->current->curr_child_id = &menu_info->current->child->id;

    if (menu_info->current->parent != NULL)
    {
        ESP_LOGI(TAG, "Deselecting id: %d", menu_info->current->id);
        menu_info->current = menu_info->current->parent;
        _menu_print(menu_info);
        return menu_info->current->id;
    }
    else
    {
        ESP_LOGI(TAG, "Deactivating menu");

        menu_deactivated_event_t event = {
            .menu_id = menu_info->menu_id};

        ESP_ERROR_CHECK(esp_event_post(MENU_MODULE_EVENT, MENU_DEACTIVATED, &event, sizeof(menu_deactivated_event_t), 100 / portTICK_RATE_MS));
        _menu_print(menu_info);
        return -1;
    }
}