#ifndef INPUT_H
#define INPUT_H

#include <esp_peripherals.h>
#include <esp_event.h>
#include <esp_err.h>
#include <mcp23017.h>
#include <qwiictwist.h>

typedef enum
{
    VOL_UP_PRESSED,
    VOL_DOWN_PRESSED,
    PLAY_PRESSED,
    SET_PRESSED,
    MENU_UP_PRESSED,
    MENU_DOWN_PRESSED,
    MENU_DESELECT_PRESSED,
    MENU_SELECT_PRESSED,
} input_module_event_t;

ESP_EVENT_DECLARE_BASE(INPUT_MODULE_EVENT);

/**
 *  @brief Initialize the input sources.
 *  @param[in] set_handle The peripherals set handle.
 *  @param[in] mcp23017_info The mcp23017 info instance.
 */
void init_inputs(esp_periph_set_handle_t set_handle, mcp23017_info_t *mcp23017_info, qwiictwist_info_t *qwiictwist_info);

#endif