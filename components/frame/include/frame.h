#ifndef FRAME_H
#define FRAME_H

#include <esp_err.h>
#include <lcd.h>

#define MAX_FRAME_WIDTH 20
#define MAX_FRAME_HEIGHT 4

/** 
 * @brief The frame draw callback, use the x and y to set the position of the cursor.
 */
typedef esp_err_t (*frame_draw_cb_t)(int x, int y, char *string, int length);

/** @struct frame
 *  @brief  This struct holds information including the buffer for a frame.
 *          It is connected to a frame group. The coordinates define where the
 *          the buffer will be written to.
 *  @param x The x coordinate.
 *  @param y The y coordinate.
 *  @param width The width.
 *  @param height The height.
 *  @param value The string buffer, this holds all the data as one sequence of characters, it does not contain a \\0 character.
 *  @param parent The parent of the frame.
 */
typedef struct frame
{
    int x;
    int y;
    int width;
    int height;
    char *value;
    struct frame_group *parent;
} frame_t;

/** @struct frame_group
 *  @brief  This struct holds information about a frame group, including the frames it contains.
 *  @param id The unique identifier.
 *  @param amount The current amount of frames.
 *  @param amount_max The max amount of frames it can hold and has allocated in memory.
 *  @param frames The memory block for pointers, pointing to a frame. Maximum is defined by amount_max
 *                and current occupation is defined by amount.
 *  @param root The root of this frame group.
 */
typedef struct frame_group
{
    int id;
    int amount;
    int amount_max;
    frame_t **frames;
    struct frame_root *root;
} frame_group_t;

/** @struct frame_root
 * @brief   This struct holds information about a frame root.
 * @param current_id The identification of the active frame group.
 * @param draw_cb The drawing callback used to draw to the target. This is abstracted away for maximum reuse.
 */
typedef struct frame_root
{
    int current_id;
    frame_draw_cb_t draw_cb;
} frame_root_t;

/**
 *  @brief  Construct a new frame root instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new frame root instance, or NULL if it cannot be created.
 */
frame_root_t *frame_root_malloc();

/**
 *  @brief  Delete an existing frame root instance.
 *  @param[in,out] menu_info Pointer to frame root instance that will be freed and set to NULL.
 */
void frame_root_free(frame_root_t **frame_root);

/**
 *  @brief  Initialize the frame root instance.
 *  @param[in] frame_root Frame root instance that will be initialized.
 *  @param[in] draw_cb Drawing callback.
 *  @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_root_init(frame_root_t *frame_root, frame_draw_cb_t draw_cb);

/**
 *  @brief  Construct a new frame group instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new frame group instance, or NULL if it cannot be created.
 */
frame_group_t *frame_group_malloc();

/**
 *  @brief  Delete an existing frame group instance.
 *  @param[in,out] menu_info Pointer to frame group instance that will be freed and set to NULL.
 */
void frame_group_free(frame_group_t **frame_group);

/**
 *  @brief  Initialize the frame group instance.
 *  @param[in] frame_group Frame group instance that will be initialized.
 *  @param[in] root Frame root for this frame group.
 *  @param[in] id Unique id of this frame group.
 *  @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_group_init(frame_group_t *frame_group, frame_root_t *root, int id);

/**
 * @brief   Add a frame to a frame group.
 * @param[in] frame_group Frame group to use.
 * @param[in] frame Frame to add to the frame group.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_group_add(frame_group_t *frame_group, frame_t *frame);

/**
 * @brief   Clears all entries of a frame group.
 * @param[in] frame_group Frame group to use.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_group_clear(frame_group_t *frame_group);

/**
 * @brief   Activate a frame group within the root it's connected to.
 *          Only one frame group can be activated per frame root.
 *          When a frame group is active, the frames within the frame group
 *          can write instantly instead of writing to the buffer only.
 * @param[in] frame_group Frame group to use.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_group_activate(frame_group_t *frame_group);

/**
 *  @brief  Construct a new frame instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new frame instance, or NULL if it cannot be created.
 */
frame_t *frame_malloc();

/**
 *  @brief  Delete an existing frame instance.
 *  @param[in,out] menu_info Pointer to frame instance that will be freed and set to NULL.
 */
void frame_free(frame_t **frame);

/**
 *  @brief  Initialize the frame instance.
 *  @param[in] frame Frame instance that will be initialized.
 *  @param[in] x x coordinate for this frame within frame group, keep this within the width.
 *  @param[in] y y coordinate for this frame within frame group, keep this within the height.
 *  @param[in] width max width for this frame, exceeding the width will cut off the text.
 *  @param[in] height max height for this frame, exceeding the height will cut off the text.
 *  @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_init(frame_t *frame, int x, int y, int width, int height);

/**
 * @brief   Write a string to a frame buffer.
 * @param[in] frame Frame to use.
 * @param[in] x x coordinate to write string to, exceeding the frame width will cut off the text.
 * @param[in] y y coordinate to write string to, exceeding the frame height will cut off the text.
 * @param[in] string The string to write to the frame buffer, exceeding the width or height will cut off the text.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_write_string(frame_t *frame, int x, int y, char *string);

/**
 * @brief   Write a character to a frame buffer.
 * @param[in] frame Frame to use.
 * @param[in] x x coordinate to write string to, exceeding the frame width will cut off the text.
 * @param[in] y y coordinate to write string to, exceeding the frame height will cut off the text.
 * @param[in] character The character to write to the frame buffer, exceeding the width or height will cut off the character.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_write_character(frame_t *frame, int x, int y, char character);

/**
 * @brief   Clear the frame buffer (aka it fills the buffer with spaces).
 * @param[in] frame Frame to use.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_clear(frame_t *frame);

/**
 * @brief   Flush the frame buffer, writing instantly if the frame group it's in is active.
 * @param[in] frame Frame to use.
 * @return Error type, should be ESP_OK when successful.
 */
esp_err_t frame_flush(frame_t *frame);

#endif