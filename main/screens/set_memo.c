#include <freertos/FreeRTOS.h>
#include <assert.h>
#include <string.h>
#include <esp_log.h>
#include <dirent.h>
#include <regex.h>

#include <input.h>
#include <set_memo.h>
#include <menu.h>
#include <audio_module.h>

#define SET_MEMO_MENU_ID_ROOT 0
#define SET_MEMO_MENU_ID 0

static const char TAG[] = "set_memo";

static void _setup(set_memo_info_t *info);
static void _teardown(set_memo_info_t *info);
static void _reset(set_memo_info_t *info);
static void _select_memo(set_memo_info_t *info, int memo_index);
static void _strip_ext(char *file);
static bool _add_memos(set_memo_info_t *info, menu_node_t *root);

static void _select_memo(set_memo_info_t *info, int memo_index)
{
    char *memo_name = info->memos[memo_index];

    if (info->mode == PLAY)
    {
        ESP_LOGI(TAG, "Play memo %s", memo_name);
        play_memo(memo_name);
    }
    else if (info->mode == REMOVE)
    {
        // TODO: remove memo here
        ESP_LOGI(TAG, "Remove memo %s", memo_name);

        char path[100];
        sprintf(path, "/sdcard/memos/%s.wav", memo_name);
        remove(path);

        _reset(info);
    }
    else
    {
        ESP_LOGE(TAG, "No mode!");
    }
}

static void _strip_ext(char *file)
{
    char *end = file + strlen(file);

    while (end > file && *end != '.')
    {
        --end;
    }

    if (end > file)
    {
        *end = '\0';
    }
}

static bool _add_memos(set_memo_info_t *info, menu_node_t *root)
{
    char **memos = (char **)malloc(sizeof(char *));
    memos[0] = NULL;

    struct dirent *files;

    DIR *directory = opendir("/sdcard/memos");
    if (directory == NULL)
    {
        ESP_LOGE(TAG, "/sdcard/memos directory cannot be opened!");
        return false;
    }

    uint8_t id = 0;
    int size = 0;

    while ((files = readdir(directory)) != NULL)
    {
        char *text = (char *)malloc(sizeof(char) * (strlen(files->d_name) + 1));
        strcpy(text, files->d_name);

        _strip_ext(text);

        ESP_LOGI(TAG, "Read %s length %d", text, strlen(text));

        menu_node_t *menu_node = menu_node_malloc();
        menu_node_init(menu_node, id++, ITEM, text);
        menu_add_item(root, menu_node);

        memos = (char **)realloc(memos, sizeof(char *) * (size + 2));
        memos[size] = text;
        memos[size + 1] = NULL;

        size++;
    }

    closedir(directory);

    info->memos = memos;

    if (size == 0)
        return false;

    return true;
}

static void _setup(set_memo_info_t *info)
{
    info->menu = menu_malloc();

    menu_node_t *root = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(root, SET_MEMO_MENU_ID_ROOT, MENU, "ROOT"));

    info->has_memos = _add_memos(info, root);
    if (!info->has_memos)
    {
        menu_node_t *no_memos_node = menu_node_malloc();
        menu_node_init(no_memos_node, -1, ITEM, "No memos");
        menu_add_item(root, no_memos_node);
    }

    ESP_ERROR_CHECK(menu_init(info->menu, root, info->menu_frame, SET_MEMO_MENU_ID));
}

static void _teardown(set_memo_info_t *info)
{
    menu_free(&info->menu);
}

static void _reset(set_memo_info_t *info)
{
    _teardown(info);
    _setup(info);
}

static esp_err_t _screen_enter(screen_t *screen)
{
    set_memo_info_t *info = (set_memo_info_t *)screen->info;

    _setup(info);

    return ESP_OK;
}

static esp_err_t _screen_exit(screen_t *screen)
{
    set_memo_info_t *info = (set_memo_info_t *)screen->info;

    _teardown(info);

    return ESP_OK;
}

static esp_err_t _screen_input(screen_t *screen, input_module_event_t event)
{
    set_memo_info_t *info = (set_memo_info_t *)screen->info;

    if (event == MENU_UP_PRESSED)
    {
        // move current memo down
        menu_previous(info->menu);
    }
    else if (event == MENU_DOWN_PRESSED)
    {
        // move current memo up
        menu_next(info->menu);
    }
    else if (event == MENU_SELECT_PRESSED)
    {
        int selected = menu_select(info->menu);

        if (info->has_memos)
        {
            // select current memo
            _select_memo(info, selected);
        }
        else
        {
            ESP_LOGI(TAG, "No memos, so do nothing");
        }
    }
    else if (event == MENU_DESELECT_PRESSED)
    {
        // exit screen
        screen_activate(main_menu_screen);
    }

    return ESP_OK;
}

screen_t *init_set_memo_screen(frame_root_t *frame_root, int id)
{
    assert(frame_root != NULL);
    assert(id >= 0);

    screen_t *screen = screen_malloc();
    frame_group_t *frame_group = frame_group_malloc();
    frame_group_init(frame_group, frame_root, id);
    screen->frame_group = frame_group;

    set_memo_info_t *info = (set_memo_info_t *)malloc(sizeof(set_memo_info_t));
    memset(info, 0, sizeof(set_memo_info_t));

    info->menu_frame = frame_malloc();
    frame_init(info->menu_frame, 0, 0, 20, 4);
    frame_group_add(screen->frame_group, info->menu_frame);

    screen->info = info;
    screen->enter_handler = _screen_enter;
    screen->exit_handler = _screen_exit;
    screen->input_handler = _screen_input;

    return screen;
}

void set_memo_screen_mode(set_memo_info_t *info, set_memo_mode_t mode)
{
    info->mode = mode;
}