#include <string.h>
#include <stdlib.h>
#include <input.h>

#include "main_menu.h"
#include <alarm.h>
#include <radio.h>
#include <set_memo.h>
#include <audio_module.h>

#define MAIN_MENU_ID 0x00

#define MAIN_MENU_ID_ROOT 0x00

#define MAIN_MENU_ID_RADIO 0x10
#define MAIN_MENU_ID_RADIO_PLAY 0x10
#define MAIN_MENU_ID_RADIO_STOP 0x11
#define MAIN_MENU_ID_RADIO_SET 0x12

#define MAIN_MENU_ID_ALARM 0x20
#define MAIN_MENU_ID_ALARM_SET 0x21
#define MAIN_MENU_ID_ALARM_REMOVE 0x22

#define MAIN_MENU_ID_MEMO 0x40
#define MAIN_MENU_ID_MEMO_RECORD 0x41
#define MAIN_MENU_ID_MEMO_PLAY 0x42
#define MAIN_MENU_ID_MEMO_REMOVE 0x43
#define MAIN_MENU_ID_MEMO_ALARM 0x44

static esp_err_t _screen_enter(screen_t *screen)
{
    main_menu_info_t *info = (main_menu_info_t *)screen->info;

    return ESP_OK;
}

static esp_err_t _screen_exit(screen_t *screen)
{
    main_menu_info_t *info = (main_menu_info_t *)screen->info;

    return ESP_OK;
}

static esp_err_t _screen_input(screen_t *screen, input_module_event_t event)
{
    main_menu_info_t *info = (main_menu_info_t *)screen->info;

    if (event == MENU_UP_PRESSED)
    {
        menu_previous(info->menu);
    }
    else if (event == MENU_DOWN_PRESSED)
    {
        menu_next(info->menu);
    }
    else if (event == MENU_SELECT_PRESSED)
    {
        int id = menu_select(info->menu);
        switch (id)
        {
        case MAIN_MENU_ID_RADIO_SET:
            screen_activate(set_radio_screen);
            break;
        case MAIN_MENU_ID_RADIO_PLAY:
            play_radio(radio_info);
            break;
        case MAIN_MENU_ID_RADIO_STOP:
            pause_radio(radio_info);
            break;
        case MAIN_MENU_ID_ALARM_SET:
            screen_activate(set_alarm_screen);
            break;
        case MAIN_MENU_ID_ALARM_REMOVE:
            remove_alarm(alarm_info);
            break;
        case MAIN_MENU_ID_MEMO_RECORD:
            record_memo();
            break;
        case MAIN_MENU_ID_MEMO_PLAY:
            set_memo_screen_mode((set_memo_info_t *)set_memo_screen->info, PLAY);
            screen_activate(set_memo_screen);
            break;
        case MAIN_MENU_ID_MEMO_REMOVE:
            set_memo_screen_mode((set_memo_info_t *)set_memo_screen->info, REMOVE);
            screen_activate(set_memo_screen);
            break;
        default:
            break;
        }
    }
    else if (event == MENU_DESELECT_PRESSED)
    {
        if (menu_deselect(info->menu) == -1)
        {
            screen_activate(idle_screen);
        }
    }

    return ESP_OK;
}

screen_t *init_main_menu_screen(frame_root_t *frame_root, int id)
{
    assert(frame_root != NULL);
    assert(id >= 0);

    screen_t *screen = screen_malloc();
    frame_group_t *frame_group = frame_group_malloc();
    frame_group_init(frame_group, frame_root, id);
    screen->frame_group = frame_group;

    main_menu_info_t *info = (main_menu_info_t *)malloc(sizeof(main_menu_info_t));
    memset(info, 0, sizeof(main_menu_info_t));

    info->menu_frame = frame_malloc();
    frame_init(info->menu_frame, 0, 0, 20, 4);
    frame_group_add(screen->frame_group, info->menu_frame);

    info->menu = menu_malloc();
    menu_node_t *root = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(root, MAIN_MENU_ID_ROOT, MENU, "ROOT"));

    menu_node_t *radio = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(radio, MAIN_MENU_ID_RADIO, MENU, "RADIO"));
    menu_add_item(root, radio);

    menu_node_t *radio_play = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(radio_play, MAIN_MENU_ID_RADIO_PLAY, ITEM, "PLAY RADIO"));
    menu_add_item(radio, radio_play);

    menu_node_t *radio_stop = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(radio_stop, MAIN_MENU_ID_RADIO_STOP, ITEM, "STOP RADIO"));
    menu_add_item(radio, radio_stop);

    menu_node_t *radio_set = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(radio_set, MAIN_MENU_ID_RADIO_SET, ITEM, "SET RADIO"));
    menu_add_item(radio, radio_set);

    menu_node_t *alarm = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(alarm, MAIN_MENU_ID_ALARM, MENU, "ALARM"));
    menu_add_item(root, alarm);

    menu_node_t *alarm_set = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(alarm_set, MAIN_MENU_ID_ALARM_SET, ITEM, "SET ALARM"));
    menu_add_item(alarm, alarm_set);

    menu_node_t *alarm_remove = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(alarm_remove, MAIN_MENU_ID_ALARM_REMOVE, ITEM, "REMOVE ALARM"));
    menu_add_item(alarm, alarm_remove);

    menu_node_t *memo = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(memo, MAIN_MENU_ID_MEMO, MENU, "MEMO"));
    menu_add_item(root, memo);

    menu_node_t *memo_record = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(memo_record, MAIN_MENU_ID_MEMO_RECORD, ITEM, "RECORD MEMO"));
    menu_add_item(memo, memo_record);

    menu_node_t *memo_play = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(memo_play, MAIN_MENU_ID_MEMO_PLAY, ITEM, "PLAY MEMO"));
    menu_add_item(memo, memo_play);

    menu_node_t *memo_remove = menu_node_malloc();
    ESP_ERROR_CHECK(menu_node_init(memo_remove, MAIN_MENU_ID_MEMO_REMOVE, ITEM, "REMOVE MEMO"));
    menu_add_item(memo, memo_remove);

    // menu_node_t *memo_alarm = menu_node_malloc();
    // ESP_ERROR_CHECK(menu_node_init(memo_alarm, MAIN_MENU_ID_MEMO_ALARM, ITEM, "SET ALARM MEMO"));
    // menu_add_item(memo, memo_alarm);

    ESP_ERROR_CHECK(menu_init(info->menu, root, info->menu_frame, MAIN_MENU_ID));

    screen->info = info;
    screen->enter_handler = _screen_enter;
    screen->exit_handler = _screen_exit;
    screen->input_handler = _screen_input;

    return screen;
}