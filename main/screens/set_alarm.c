#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <time.h>
#include <freertos/FreeRTOS.h>
#include <assert.h>
#include <string.h>
#include <esp_log.h>

#include <input.h>
#include <set_alarm.h>

static const char TAG[] = "set_alarm";

static void _digit_up(set_alarm_data_t *data)
{
    ESP_LOGV(TAG, "digit up");

    int cursor = data->cursor;
    int digit = data->digits[cursor];

    if ((cursor == 0 && digit == 23) || (cursor == 1 && digit == 59))
    {
        digit = 0;
    }
    else
    {
        digit++;
    }

    data->digits[cursor] = digit;
}

static void _digit_down(set_alarm_data_t *data)
{
    ESP_LOGV(TAG, "digit down");

    int cursor = data->cursor;
    int digit = data->digits[cursor];

    if (cursor == 0 && digit == 0)
    {
        digit = 23;
    }
    else if (cursor == 1 && digit == 0)
    {
        digit = 59;
    }
    else
    {
        digit--;
    }

    data->digits[cursor] = digit;
}

static bool _digit_next(set_alarm_data_t *data)
{
    ESP_LOGV(TAG, "digit next");

    if (data->cursor == 1)
    {
        return true;
    }

    data->cursor = data->cursor + 1;
    return false;
}

static void _draw_digits(set_alarm_info_t *info)
{
    char value[10];

    ESP_LOGV(TAG, "Print d1: %d", info->data->digits[0]);
    ESP_LOGV(TAG, "Print d2: %d", info->data->digits[1]);

    sprintf(value, "%02d:%02d", info->data->digits[0], info->data->digits[1]);
    frame_write_string(info->value_frame, 0, 0, value);
    frame_flush(info->value_frame);
}

static void _draw_cursor(set_alarm_info_t *info)
{
    int cursor = 0;

    if (info->data->cursor == 0)
    {
        cursor = 0;
    }
    else if (info->data->cursor == 1)
    {
        cursor = 3;
    }

    frame_clear(info->cursor_frame);
    frame_write_string(info->cursor_frame, cursor, 0, "^^");
    frame_flush(info->cursor_frame);
}

static void _draw_ok(set_alarm_info_t *info)
{
    frame_clear(info->cursor_frame);
    frame_flush(info->cursor_frame);
    frame_clear(info->ok_frame);
    frame_write_string(info->ok_frame, 0, 0, "OK?");
    frame_write_string(info->ok_frame, 0, 1, "^^^");
    frame_flush(info->ok_frame);
}

static void _set_alarm(set_alarm_info_t *info)
{
    ESP_LOGV(TAG, "set alarm");

    struct tm time_info = {
        .tm_hour = info->data->digits[0],
        .tm_min = info->data->digits[1],
    };

    set_alarm(info->alarm_info, &time_info);
}

static esp_err_t _screen_enter(screen_t *screen)
{
    set_alarm_info_t *info = (set_alarm_info_t *) screen->info;

    info->label_frame = frame_malloc();
    frame_init(info->label_frame, 2, 1, 18, 1);
    frame_group_add(screen->frame_group, info->label_frame);
    frame_write_string(info->label_frame, 0, 0, "Set time:");
    frame_flush(info->label_frame);

    info->value_frame = frame_malloc();
    frame_init(info->value_frame, 2, 2, 5, 1);
    frame_group_add(screen->frame_group, info->value_frame);
    frame_write_string(info->value_frame, 0, 0, "00:00");
    frame_flush(info->value_frame);

    info->cursor_frame = frame_malloc();
    frame_init(info->cursor_frame, 2, 3, 18, 1);
    frame_group_add(screen->frame_group, info->cursor_frame);
    frame_write_string(info->cursor_frame, 0, 0, "^^");
    frame_flush(info->cursor_frame);

    info->ok_frame = frame_malloc();
    frame_init(info->ok_frame, 8, 2, 3, 2);
    frame_group_add(screen->frame_group, info->ok_frame);

    set_alarm_data_t *data = (set_alarm_data_t *)malloc(sizeof(set_alarm_data_t));
    memset(data, 0, sizeof(set_alarm_data_t));

    data->cursor = 0;
    data->digits = (int *)malloc(sizeof(int) * 2);
    data->digits[0] = 0;
    data->digits[1] = 0;

    info->data = data;

    return ESP_OK;
}

static esp_err_t _screen_exit(screen_t *screen)
{
    set_alarm_info_t *info = (set_alarm_info_t *)screen->info;

    frame_group_clear(screen->frame_group);
    free(info->data->digits);
    free(info->data);

    return ESP_OK;
}

static esp_err_t _screen_input(screen_t *screen, input_module_event_t event)
{
    set_alarm_info_t *info = (set_alarm_info_t *)screen->info;

    if (event == MENU_UP_PRESSED && !info->data->ok_showed)
    {
        // move current digit up
        _digit_up(info->data);
        _draw_digits(info);
    }
    else if (event == MENU_DOWN_PRESSED && !info->data->ok_showed)
    {
        // move current digit down
        _digit_down(info->data);
        _draw_digits(info);
    }
    else if (event == MENU_SELECT_PRESSED)
    {
        // move toward next digit
        if (_digit_next(info->data))
        {
            // end of digits

            if (!info->data->ok_showed)
            {
                _draw_ok(info);
                info->data->ok_showed = true;
            }
            else if (!info->data->confirmed)
            {
                _set_alarm(info);
                info->data->confirmed = true;
                screen_activate(main_menu_screen);
            }
        }
        else
        {
            _draw_cursor(info);
        }
    }
    else if (event == MENU_DESELECT_PRESSED)
    {
        // exit screen
        screen_activate(main_menu_screen);
    }

    return ESP_OK;
}

screen_t *init_set_alarm_screen(frame_root_t *frame_root, int id, alarm_info_t *alarm_info)
{
    assert(frame_root != NULL);
    assert(id >= 0);
    assert(alarm_info != NULL);
    assert(alarm_info->frame != NULL);

    screen_t *screen = screen_malloc();
    frame_group_t *frame_group = frame_group_malloc();
    frame_group_init(frame_group, frame_root, id);
    screen->frame_group = frame_group;

    set_alarm_info_t *info = (set_alarm_info_t *)malloc(sizeof(set_alarm_info_t));
    memset(info, 0, sizeof(set_alarm_info_t));
    info->alarm_info = alarm_info;

    screen->info = info;
    screen->enter_handler = _screen_enter;
    screen->exit_handler = _screen_exit;
    screen->input_handler = _screen_input;

    return screen;
}