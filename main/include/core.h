#ifndef MAIN_CORE_H
#define MAIN_CORE_H

#include <input.h>
#include <frame.h>
#include <alarm.h>
#include <radio.h>

/**
 * @brief This is the type of the screen.
 * 
 */
typedef struct screen screen_t;

/**
 * @brief Handles the user-input on the screen.
 * 
 */
typedef esp_err_t (*input_handler_t)(screen_t *screen, input_module_event_t event);

/**
 * @brief Handles the events that occur when a user enters this screen.
 * 
 */
typedef esp_err_t (*enter_handler_t)(screen_t *screen);

/**
 * @brief Handles the events that occur when the user exits this screen.
 * 
 */
typedef esp_err_t (*exit_handler_t)(screen_t *screen);

/**
 * @struct screen
 * @brief Data object that is used for menu tabs, stores handlers and frames.
 * 
 * @param input_handler The input handler.
 * @param enter_handler The enter_handler.
 * @param exit_handler The exit_handler.
 * @param frame_group The group of frames that gets drawn on the screen.
 * @param info ??
 */
struct screen {
    input_handler_t input_handler;
    enter_handler_t enter_handler;
    exit_handler_t exit_handler;
    frame_group_t *frame_group;
    void *info;
};

extern alarm_info_t *alarm_info;
extern radio_info_t *radio_info;

extern screen_t *idle_screen;
extern screen_t *main_menu_screen;
extern screen_t *set_alarm_screen;
extern screen_t *set_radio_screen;
extern screen_t *set_memo_screen;
extern radio_info_t *radio_info;

/**
 * @brief  Construct a new screen instance.
 *          New instance should be initialised before calling other functions.
 *  @return Pointer to new screen instance, or NULL if it cannot be created.
 */
screen_t *screen_malloc();

/**
 * @brief  Delete an existing screen instance.
 *  @param screen Pointer to screen instance that will be freed and set to NULL.
 */
void screen_free(screen_t **screen);

/**
 * @brief Changes the current screen to the screen given screen.
 * 
 * @param screen Pointer to screen instance.
 */
void screen_activate(screen_t *screen);

/**
 * @brief Sets all the data objects of the project, also initializes all the screens and registers to the needed events.
 * 
 */
void setup_core();

#endif