#ifndef MAIN_SET_ALARM_H
#define MAIN_SET_ALARM_H

#include <core.h>
#include <alarm.h>

/**
 * @struct set_alarm_data_t
 * @brief contains the data of the alarm, like the time and if it has been showed or not.
 * @param cursor Place of the cursor.
 * @param digits Current digits on the alarm.
 * @param ok_showed Boolean which is used to check if the ok message is showing on the screen.
 * @param confirmed Boolean which is used to check if the alarm is confirmed.
 * 
 */
typedef struct 
{
    int cursor;
    int *digits;
    bool ok_showed;
    bool confirmed;
} set_alarm_data_t;

/**
 * @struct set_alarm_info_t
 * @brief this contains the data of the set alarm screen. 
 * 
 * @param data Data object of the set_alarm_module
 * @param alarm_info Data object of the alarm_module
 * @param label_frame Frame which is used to show the label.
 * @param value_frame Frame which is used to show the value of the alarm.
 * @param cursor_frame Frame which is used to show the selected field on the screen.
 * @param ok_frame Frame which is used to show that the user selcted the alarm, and needs to confirm their choice.
 * 
 */
typedef struct 
{
    set_alarm_data_t *data;
    alarm_info_t *alarm_info;
    frame_t *label_frame;
    frame_t *value_frame;
    frame_t *cursor_frame;
    frame_t *ok_frame;
} set_alarm_info_t;

/**
 * @brief initlialises the set alarm screen to be able to set an alarm.
 * 
 * @param frame_root Root object of the frame structure
 * @param id Unique id of the frame_group
 * @param alarm_info contains the data of the set alarm screen.
 * @return Instance of screen object. 
 */
screen_t *init_set_alarm_screen(frame_root_t *frame_root, int id, alarm_info_t *alarm_info);

#endif