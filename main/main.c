#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include "setup.h"
#include "core.h"
#include <esp_event.h>

#include <main.h>

static void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);

static const char *TAG = "Main";

void app_main(void)
{
    setup_components();
    setup_core();
}
