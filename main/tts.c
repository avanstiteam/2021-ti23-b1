#define LOG_LOCAL_LEVEL ESP_LOG_VERBOSE

#include <audio_module.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <esp_log.h>

static const char *TAG = "TTS module";

static int _get_array_length(char **text)
{
    int i = 0;
    while (text[i] != NULL)
    {
        ESP_LOGD(TAG, "In while loop of _get_array_length(): i is %d", i);
        i++;
    }
    return i;
}

static char **_add_string(char **text, char *string)
{
    ESP_LOGD(TAG, "Adding string...");
    int array_length = _get_array_length(text);
    text = (char **)realloc(text, sizeof(char *) * (array_length + 2));
    text[array_length] = string;
    text[array_length + 1] = NULL;
    return text;
}

static char *_convert_int_to_string(int value)
{
    char *string = (char *)malloc(snprintf(NULL, 0, "voice/%d", value) + 1);
    sprintf(string, "voice/%d", value);

    return string;
}

static char **_get_number(char **text, int value)
{
    if (value < 0)
    {
        text = _add_string(text, "voice/minus");
        value *= -1;
    }
    if (value <= 15)
    {
        text = _add_string(text, _convert_int_to_string(value));
    }
    else if (value > 15 && value < 20)
    {
        int digit = value % 10;
        text = _add_string(text, _convert_int_to_string(digit));
        text = _add_string(text, "voice/teen");
    }
    else if (value >= 20)
    {
        int digit = value % 10;
        int digitTwo = value - digit;

        text = _add_string(text, _convert_int_to_string(digitTwo));
        text = _add_string(text, _convert_int_to_string(digit));
    }
    return text;
}

static void _print_text(char **text)
{
    ESP_LOGD(TAG, "Printing text...");
    int array_length = _get_array_length(text);
    for (int i = 0; i < array_length; i++)
    {
        ESP_LOGD(TAG, "Text %d: %s", i, text[i]);
    }
}

void play_time(struct tm time)
{
    char **text = (char **)malloc(sizeof(char *));
    // memset(text, 0, sizeof(char *));
    text[0] = NULL;

    text = _add_string(text, "voice/thetimeis");
    text = _get_number(text, time.tm_hour % 12);
    text = _get_number(text, time.tm_min);

    bool isPM = time.tm_hour > 12;
    text = _add_string(text, isPM ? "voice/pm" : "voice/am");

    _print_text(text);

    play_text_to_speach(text);
}

void play_temperature(int temperature)
{
    char **text = (char **)malloc(sizeof(char *));
    // memset(text, 0, sizeof(char *));
    text[0] = NULL;

    text = _add_string(text, "voice/thetempis");
    text = _get_number(text, temperature);
    text = _add_string(text, "voice/degrees");
    text = _add_string(text, "voice/celcius");

    _print_text(text);

    play_text_to_speach(text);
}
